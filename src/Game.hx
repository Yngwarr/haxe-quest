typedef Word = {
    synonym: Array<String>
}

enum Cmd {
    Unknown;
    Jump;
    Squats;
    Die;
}

enum CmdResult {
    Nothing;
    Go(room: Room);
    AddStr(value: Int);
    AddInt(value: Int);
    Death;
}

class Character {
    public function new() {}

    public var strength: Int;
    public var intellect: Int;

    public function statsStr() : String {
        return 'str: $strength, int: $intellect';
    }
}

class Item {
    public var name: Word;
}

class Room {
    public var name: Word;
    public var links: Array<Room>;
    public var items: Array<Item>;
}

class Game {
    static function print(line: String, newLine = true) {
        Sys.stdout().writeString(line);
        if (newLine) {
            Sys.stdout().writeString("\n");
        }
    }

    static function readLine() : String {
        return Sys.stdin().readLine();
    }

    static function readCmd(line: String) : Cmd {
        return switch line {
            case "jump": Jump;
            case "do squats": Squats;
            case "die": Die;
            case _: Unknown;
        }
    }

    static function evalCmd(cmd: Cmd): CmdResult {
        switch cmd {
            case Jump:
                print("You jumped and plummeted to the floor.");
                return Nothing;
            case Squats:
                print("You decide to do some squats and become stronger.");
                return AddStr(1);
            case Die:
                print("You died. :(");
                return Death;
            case _:
                print("I don't understand this command.");
                return Nothing;
        }
    }

    static function changeState(res: CmdResult, ch: Character) {
        switch res {
            case AddStr(value):
                ++ch.strength;
                print(ch.statsStr());
            case AddInt(value):
                ++ch.intellect;
                print(ch.statsStr());
            case _:
                return;
        }
    }

    static function main() {
        var character = new Character();

        while (true) {
            print("You're standing in an empty room.");

            var cmd = readCmd(readLine());
            var res = evalCmd(cmd);
            changeState(res, character);

            if (res == Death) break;
        }
    }
}
