.PHONY: build run

build:
		haxe compile.hxml
run: build
		hl a.hl
clean:
		rm a.hl
